Unity3D CAVE
============
Eine Zusammenstellung aller benötigten Tools um eine CAVE mit bis zu drei Seiten bespielen zu können. Enthält sowohl Stereo3D als auch Tracking.

## Benötigte Tools

### DTrack 2

Steuertool des ART-Controllers, Dokumentation vgl. VR / AR - Projekt
### VRPN

Stellt Verbindung zwischen OSVR und DTrack her, muss nur auf dem Steuerrechner gestartet werden
### OSVR

Fängt alle Daten aus VRPN ab und stellt sie in einer standardisierten Form zur Verfügung, sodass diese einfach in Unity verarbeitet werden können. 
### Unity3D (zuletzt getestet mit 5.3.4)

Genutzte Engine, frei verfügbar 

Ablauf für bereits erstellte Szenen
--------------
* DTrack starten und mit Controller verbinden
* VRPN starten
* DTrack Tracking starten
* OSVR mit Config starten
* Unity-Szene starten mit Paramenter -vrmode stereo
* Je nach CAVE-Seite die korrekte Kamera aktivieren ("r" für Rechts", "l" für Links oder "f" für Front drücken)

Ablauf zum Erstellen von Szenen in Unity
--------------
Projekt anlegen und Inhalte erstellen
Das Paket *CavePackage.unitypackage* aus dem Ordner *Unity Package* importieren und das "CAVE"-Prefab aus dem Ordner "Prefabs" in die Szene ziehen. Je nach OSVR-Config müssen die Pfade der Tracker angepasst werden, das Prefab ist auf die enthaltene Config abgestimmt.

Active Stereo3D in Unity
--------------
### Voraussetzungen:

* Quadro GPU
* Windows 8+ (in der Theorie, nur getestet unter Windows 10)
* aktivierte DX11 3D-Fähigkeit (zum Test: [Stereo 3D test](https://www.microsoft.com/de-de/store/apps/stereo-3d-test/9nblggh5jz4h) aus dem Windows App-Store)

### Einstellungen:

Zunächst müssen unter *Edit -> Project Settings -> Player -> Other Settings* die Optionen *Stereoscopic rendering* sowie *Virtual Reality Supported* aktiviert werden. Die Graphics API for Windows muss Direct3D11 sein, da das aktive Stereo von Unity auf DX11 aufbaut. 

Besonders wichtig ist die Option *Color Space*, welche auf *Gamma* gestellt werden muss, da sonst nur eine leere Szene zu sehen ist.

Zum Ausführen wird die Szene mit den Parametern -vrmode stereo aufgerufen.


Bekannte Fehler & Lösungen
--------------
* Probleme mit Schattenwurf aufgrund der Kameratransformationen -> Schatten des Directional Light deaktivieren
* Bild auf dem Kopf -> Kameras um -180° auf Z-Achse drehen

