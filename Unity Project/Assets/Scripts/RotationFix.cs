﻿using UnityEngine;
using System.Collections;

public class RotationFix : MonoBehaviour {

    Quaternion rotation;
    float x;
    float y;
    float z;


    // Use this for initialization
    void Start () {

        rotation = transform.rotation;
        x = rotation.x;
        y = rotation.y;
        z = rotation.z;

               
        
	
	}
	
	// Update is called once per frame
	void Update () {

        transform.rotation = Quaternion.Euler(x, -y, -z);

    }
}
