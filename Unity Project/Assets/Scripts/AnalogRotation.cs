﻿using UnityEngine;
using System.Collections;

public class AnalogRotation : OSVR.Unity.RequiresAnalogInterface
{
    public float velocity;

    // Use this for initialization
    void Start()
    {
        this.Interface.StateChanged += Interface_StateChanged;
    }

    void Update()
    {
        if (velocity != 0)
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * velocity * 100);
        }
    }


    void Interface_StateChanged(object sender, OSVR.ClientKit.TimeValue timestamp, int sensor, double report)
    {
        Debug.Log("[OSVR-Unity-Samples] Got analog value " + report);
        velocity = (float)report;
    }
}