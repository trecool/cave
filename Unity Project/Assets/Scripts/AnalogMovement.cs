﻿using UnityEngine;
using System.Collections;

public class AnalogMovement : OSVR.Unity.RequiresAnalogInterface
{
    public float velocity;

    // Use this for initialization
    void Start()
    {
        this.Interface.StateChanged += Interface_StateChanged;
    }

    void Update()
    {
        if (velocity != 0)
        {
            transform.position += transform.forward * Time.deltaTime * velocity * 3;
        }
    }


    void Interface_StateChanged(object sender, OSVR.ClientKit.TimeValue timestamp, int sensor, double report)
    {
        Debug.Log("[OSVR-Unity-Samples] Got analog value " + report);
        velocity = (float)report;        
    }
}