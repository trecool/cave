﻿using UnityEngine;
using System.Collections;

public class CameraSwitcher : MonoBehaviour {

   public GameObject cameraFront;
   public GameObject cameraRight;
   public GameObject cameraLeft;


	// Use this for initialization
	void Start () {

       
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("l"))
        {
            cameraSwitcher(cameraLeft);
        }

        if (Input.GetKeyDown("f"))
        {
            cameraSwitcher(cameraFront);
        }

        if (Input.GetKeyDown("r"))
        {
            cameraSwitcher(cameraRight);
        }

    }

    void cameraSwitcher(GameObject camera)
    {
        
        if(camera == cameraLeft)
        {
            camera.SetActive(true);
            cameraFront.SetActive(false);
            cameraRight.SetActive(false);
            return;
        }

        if (camera == cameraRight)
        {
            camera.SetActive(true);
            cameraFront.SetActive(false);
            cameraLeft.SetActive(false);
            return;
        }

        if (camera == cameraFront)
        {
            camera.SetActive(true);
            cameraLeft.SetActive(false);
            cameraRight.SetActive(false);
            return;
        }

    }
}
